({
	handleConfirmDialogYes : function(component, event, helper) {
        component.set("v.confirm", true);
        component.find("overlayLib").notifyClose();
    },   
    handleConfirmDialogNo : function(component, event, helper) {
        component.set("v.confirm", false);
        component.find("overlayLib").notifyClose();
    }
})